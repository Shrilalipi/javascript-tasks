/**
 * @todo Calculate the sum of the digits of a number
 */

// const sumDigitsOfNumber = function () {};
// function sumDigitsOfNumber() {}
// const sumDigitsOfNumber = (each) => {};

// function sumDigitsOfNumber(number) {
//   // Ensure the input is a non-negative integer
//   if (!Number.isInteger(number) || number < 0) {
//     return "Invalid input. Please provide a non-negative integer.";
//   }

//   // Convert the number to a string to iterate through its digits
//   const numString = number.toString();

//   // Initialize the sum
//   let sum = 0;

//   // Iterate through each digit and add it to the sum
//   for (let i = 0; i < numString.length; i++) {
//     sum += parseInt(numString[i]);
//   }

//   return sum;
// }

// // Example usage
// const number = 12345;
// const result = sumDigitsOfNumber(number);
// console.log(`The sum of digits of ${number} is: ${result}`);

let num = 346;
let sum = 0;
console.log(num % 10);
console.log(parseInt(num / 10));

while (num > 0) {
  let rem = num % 10;
  sum = sum + rem;
  num = parseInt(num / 10);
}
console.log(sum);
