const students = [
  { name: "John", age: 20 },
  { name: "Jane", age: 17 },
  { name: "Bob", age: 22 },
  { name: "Alice", age: 16 },
];
// const studentsBelow18 = students.filter((student) => student.age < 18);
let studentsName = students.map((student) => {
  if (student.age >= 20) {
    return student.name;
  }
});

studentsName = students.filter((student) =>
  student.age >= 20 ? student.name : undefined
);

// console.log("Students below 18 years old:", studentsBelow18);
console.log("Students Above 18 years old:", studentsName);
