/**
 * @todo Find the factorial of a number
*/

const findFactorial =  (number) => {
    let num = number;
    let fact = 1;
    if (num === 0 || num === 1) {
        fact = 1;
    } else {
        let res =  findFactorial(num - 1);
        fact = num * res;
    }
    return fact;
}


let num = 5;
let factorialOfNumber =  findFactorial(num);
console.log(`factorial of ${num} is: `, factorialOfNumber);

