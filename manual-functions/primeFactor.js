function primeFactors(num) {
  const factors = [];

  // Divide the number by 2 until it is odd
  while (num % 2 === 0) {
    factors.push(2);
    num /= 2;
  }

  // Check for odd prime factors starting from 3
  for (let i = 3; i <= Math.sqrt(num); i += 2) {
    while (num % i === 0) {
      factors.push(i);
      num /= i;
    }
  }

  // If the remaining number is a prime greater than 2
  if (num > 2) {
    factors.push(num);
  }

  return factors;
}

// Example usage
const numberToFactorize = 84;
const result = primeFactors(numberToFactorize);

console.log(`Prime factors of ${numberToFactorize}: ${result.join(", ")}`);
