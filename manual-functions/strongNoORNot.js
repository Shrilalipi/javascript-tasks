function factorial(n) {
  if (n === 0 || n === 1) {
    return 1;
  } else {
    return n * factorial(n - 1);
  }
}

function isStrongNumber(num) {
  // Convert the number to an array of digits
  const digits = Array.from(String(num), Number);

  // Calculate the sum of factorials of individual digits
  const sumOfFactorials = digits.reduce(
    (sum, digit) => sum + factorial(digit),
    0
  );

  // Check if the sum is equal to the original number
  return sumOfFactorials === num;
}

// Example usage
const numberToCheck = 145;
if (isStrongNumber(numberToCheck)) {
  console.log(`${numberToCheck} is a strong number.`);
} else {
  console.log(`${numberToCheck} is not a strong number.`);
}
