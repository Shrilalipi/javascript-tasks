function isPalindromeNumber(num) {
  const numStr = num.toString();
  const reversedNumStr = numStr.split("").reverse().join("");

  return numStr === reversedNumStr;
}

// Example usage
const numberToCheck = 121;
if (isPalindromeNumber(numberToCheck)) {
  console.log(`${numberToCheck} is a palindrome number.`);
} else {
  console.log(`${numberToCheck} is not a palindrome number.`);
}
