function reverseNumber(number) {
  let reversed = 0;

  while (number !== 0) {
    let digit = number % 10;

    reversed = reversed * 10 + digit;

    number = Math.floor(number / 10);
  }

  return reversed;
}

let originalNumber = 12345;
let reversedNumber = reverseNumber(originalNumber);

console.log(`Original Number: ${originalNumber}`);
console.log(`Reversed Number: ${reversedNumber}`);
